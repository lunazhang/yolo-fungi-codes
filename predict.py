#! /usr/bin/env python

import argparse
import os
import cv2
import numpy as np
from tqdm import tqdm
from preprocessing import parse_annotation
from utils import draw_boxes
from frontend import YOLO
import json
import matplotlib.pyplot as plt
from PIL import Image
import time

os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"

argparser = argparse.ArgumentParser(
    description='Train and validate YOLO_v2 model on any dataset')

argparser.add_argument(
    '-c',
    '--conf',
    help='path to configuration file')

argparser.add_argument(
    '-w',
    '--weights',
    help='path to pretrained weights')

argparser.add_argument(
    '-i',
    '--input',
    help='path to an image or an video (mp4 format)'
    , required=False)

def _main_(args):
    start = time.time()
    config_path  = args.conf
    weights_path = args.weights
    #image_path   = args.input

    with open(config_path) as config_buffer:    
        config = json.load(config_buffer)

    ###############################
    #   Make the model 
    ###############################

    yolo = YOLO(backend             = config['model']['backend'],
                input_size          = config['model']['input_size'], 
                labels              = config['model']['labels'], 
                max_box_per_image   = config['model']['max_box_per_image'],
                anchors             = config['model']['anchors'])

    ###############################
    #   Load trained weights
    ###############################    

    yolo.load_weights(weights_path)

    ###############################
    #   Predict bounding boxes 
    ###############################
    #print(len(boxes), 'boxes are found')
    #print(type(image))
    #cv2.imwrite(image_path[:-4] + '_detected' + image_path[-4:], image)
  #  ctr = 0
    dir_list = os.listdir('/home/revealbio/Documents/LUNA/Fungi_Images/7_16_18/Tiles/')
    # for i in range(10):
    #     print(dir_list[i])
    # newImage = cv2.imread('/home/revealbio/Downloads/Fungi/better_data/train/' + dir_list[len(dir_list) - 1])
    # plt.imshow(newImage[:, :, ::-1]);
    # plt.show()

    with open('/home/revealbio/Documents/LUNA/Fungi_Images/7_16_18/results.txt', 'w') as file:
        file.write("image name, xmin, xmax, ymin, ymax, confidence\n")

        for elem in dir_list:
            image = cv2.imread('/home/revealbio/Documents/LUNA/Fungi_Images/7_16_18/Tiles/' + elem)
            ctr = 0

            print("image: %s" % elem)
            boxes = yolo.predict(image)

            for el in boxes:
                ctr += 1
                print("box: " + str(ctr))
                el.print_stuff(image)

            if len(boxes) > 0:
                image, x_min, y_min, x_max, y_max, box_score = draw_boxes(image, boxes, config['model']['labels'])
                # write results to a text file

                for i in range(0, len(x_min)):
                    file.write("{}, {}, {}, {}, {}, {}\n".format(elem, x_min[i], x_max[i], y_min[i], y_max[i], round(box_score[i], 2)))
            else:
                print("no boxes detected")
                file.write("{}, {}\n".format(elem, "no boxes detected"))

            cv2.imwrite('/home/revealbio/Documents/LUNA/Fungi_Images/7_16_18/predictions/' + elem, image)

    end = time.time()
    elapsed_time = end - start
    print('elapsed time (sec): %f' % elapsed_time)

if __name__ == '__main__':
    args = argparser.parse_args()
    _main_(args)
